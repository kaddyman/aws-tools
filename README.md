# aws-tools

Just some scripts I made for finding s3 bucket size, ec2 instance-event's and other tools. still in the process of rewriting most of these.

### list s3 files in public bucket

```
aws s3 ls s3://some-bucket-sure/ --no-sign-request
```
